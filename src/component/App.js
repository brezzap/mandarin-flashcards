import React, {Component} from 'react';
import './App.scss';
import {sentencePieces, sentences} from "../lib/sentences";
import {SentenceCardSupplier} from "../lib/sentence-card-supplier";

// Based heavily on https://codepen.io/mattgreenberg/pen/ggOpOr

// TODO: Way to choose things to study (numbers, dates, lesson 1, etc)
// TODO: Remember the front/back setting (and other settings... the things to study, etc)
// TODO: Auto-generate cards
//          - Numbers/dates
//          - Sentences
//          - Needs a way to balance against other things
// TODO: Variable speed speech
//          - Could be like Google, where the second play is slower
// TODO: Automatic pinyin generation
//          - Hard because a bunch of characters have multiple pinyin
//          - https://github.com/yannickmahe/HanziList
//          - https://github.com/nieldlr/hanzi
//          - https://github.com/quizlet/pinyin-converter
// TODO: Support multiple Chinese translations
//          - Will help most with Chinese -> English
//          - Read one of X chinese versions and the back of the card is the English
//          - Days of the week is the big example


class Card extends React.Component {

    constructor() {
        super();
        this.state = {
            showBack: false
        }
    }

    showFront() {
        this.setState({ showBack: false });
    }

    isShowingFront() {
        return !this.state.showBack;
    }

    flip() {
        this.setState({ showBack: !this.state.showBack });
    }

    render() {
        const content = this.state.showBack ? this.props.backContent : this.props.frontContent;

        return (
            <div
                className="card"
                onClick={() => {
                    // Prevent flipping if the user is selecting text
                    if (window.getSelection().type !== "Range") {
                        this.flip();
                    }
                }}
            >
                <div className="card-content">
                    {content}
                </div>
            </div>
        );
    }
}

function Button(props) {
    return (
        <div
            className='button'
            onClick={props.action}
        >
            {props.children}
        </div>
    )
}

function Header(props) {
    return (
        <div className='header'>
            <div className='header-content'>
                {props.children}
            </div>
        </div>
    )
}

function MultiLineText(props) {
    // I have to use <React.Fragment> here instead of <> so I can put a key on each item
    return <div className={props.className}>{props.text.split("\n").map((line, idx) => <React.Fragment key={idx}>{line}<br/></React.Fragment>)}</div>;
}

class Settings extends Component {
    render() {
        return (
            <div className="settings">
                <div className="title">
                    Settings
                </div>
                <div
                    className='close-button'
                    onClick={this.props.onClose}
                >
                    x
                </div>
                <div className='setting'>
                    <div className='setting-name'>Card style</div>
                    <label>
                        <input type="radio" name="card-style"
                               value='english-front'
                               checked={this.props.cardStyle === 'english-front'}
                               onChange={this.props.onCardStyleChanged}/>
                        English front / Chinese back
                    </label>
                    <label>
                        <input type="radio" name="card-style"
                               value='chinese-front'
                               checked={this.props.cardStyle === 'chinese-front'}
                               onChange={this.props.onCardStyleChanged}/>
                        Chinese front / English back
                    </label>
                    <label>
                        <input type="radio" name="card-style"
                               value='speech-front'
                               checked={this.props.cardStyle === 'speech-front'}
                               onChange={this.props.onCardStyleChanged}/>
                        Speech front / Chinese & English back
                    </label>
                </div>
            </div>
        );
    }
}

class CardController extends Component {

    static MANDARIN_LANG = "zh-Hans";

    constructor(props) {
        super(props);

        this.child = React.createRef();

        this.boundFlipCard = this.flipCard.bind(this);
        this.boundHandleKeyDown = this.handleKeyDown.bind(this);
        this.boundShowNewCard = this.showNewCard.bind(this);
        this.boundShowSettings = this.showSettings.bind(this);
        this.boundSpeakCard = this.speakCard.bind(this);

        this.state = {
            card: this.props.cardSupplier.getNextCard(),
            cardStyle: 'chinese-front',
        }
    }

    componentWillMount() {
        document.addEventListener("keydown", this.boundHandleKeyDown, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.boundHandleKeyDown, false);
    }

    handleKeyDown(event) {
        // Ignore repeat events and events with modifier keys pressed (shift is OK)
        if (event.repeat || event.altKey || event.ctrlKey || event.metaKey) {
            return;
        }

        switch (event.key.toLowerCase()) {
            case "n":
            case "right": // IE/Edge specific
            case "arrowright":
                this.showNewCard();
                break;
            case "f":
            case "left": // IE/Edge specific
            case "arrowleft":
                this.flipCard();
                break;
            case "s":
            case "enter":
            case "up": // IE/Edge specific
            case "arrowup":
                this.speakCard();
                break;
            case " ":
            case "down": // IE/Edge specific
            case "arrowdown":
                this.advance();
                break;
            default:
                // do nothing
                break;
        }
    }

    showNewCard() {
        this.setState({
            card: this.props.cardSupplier.getNextCard(),
        });
        this.child.current.showFront();

        if (this.state.cardStyle === 'speech-front') {
            this.speakCard();
        }
    }

    flipCard() {
        this.child.current.flip();

        if (this.child.current.isShowingFront() && this.state.cardStyle === 'speech-front') {
            this.speakCard();
        }
    }

    // Flips to the back or advances to the next card if it's already showing the back
    advance() {
        if (this.child.current.isShowingFront()) {
            this.flipCard();
        } else {
            this.showNewCard();
        }
    }

    showSettings() {
        this.setState({
            showSettings: true,
        });
    }

    hideSettings() {
        this.setState({
            showSettings: false,
        });
    }

    handleChange(event) {
        this.setState({
            size: event.target.value
        });
    }

    onCardStyleChanged = event => {
        this.setState({
            cardStyle: event.target.value,
        })
    };

    speakCard() {
        if (speechSynthesis.speaking) {
            speechSynthesis.cancel()
        }
        const speech = new SpeechSynthesisUtterance();
        speech.text = this.state.card.chinese;
        speech.volume = 1; // 0 to 1
        //TOOD: support changing the speech speed
        speech.rate = 0.8; // 0.1 to 9
        speech.pitch = 1; // 0 to 2, 1=normal
        speech.lang = CardController.MANDARIN_LANG;
        speechSynthesis.speak(speech);
    }

    renderCardFront(card) {
        switch (this.state.cardStyle) {
            case 'chinese-front':
                return <MultiLineText className="headline chinese" text={card.chinese}/>;

            case 'speech-front':
                return <div className='headline'>(speak)</div>;

            default: // english-front
                return <MultiLineText className="headline english" text={card.english}/>;
        }
    }

    renderCardBack(card) {
        switch (this.state.cardStyle) {
            case 'chinese-front':
                return <>
                    <MultiLineText className="headline english" text={card.english}/>
                    {card.pinyin ? <div className="pinyin">{card.pinyin}</div> : ''}
                    {card.notes ? <div className="notes">{card.notes}</div> : ''}
                </>;

            case 'speech-front':
                return <>
                    <MultiLineText className="headline chinese" text={card.chinese}/>
                    <MultiLineText className="english" text={card.english}/>
                    {card.pinyin ? <div className="pinyin">{card.pinyin}</div> : ''}
                    {card.notes ? <div className="notes">{card.notes}</div> : ''}
                </>;

            default: // english-front
                return <>
                    <MultiLineText className="headline chinese" text={card.chinese}/>
                    {card.pinyin ? <div className="pinyin">{card.pinyin}</div> : ''}
                    {card.notes ? <div className="notes">{card.notes}</div> : ''}
                </>;
        }
    }


    render() {
        const card = this.state.card;

        const cardFront = this.renderCardFront(card);

        const cardBack = this.renderCardBack(card);

        return (
            <div className='wrapper'>
                <Header>
                    <Button action={this.boundSpeakCard}>Speak</Button>
                    <Button action={this.boundFlipCard}>Flip</Button>
                    <Button action={this.boundShowNewCard}>Next</Button>
                    <Button action={this.boundShowSettings}>Settings</Button>
                </Header>
                <div className='content-wrapper'>
                    {this.state.showSettings ?
                        <Settings
                            onClose={() => this.hideSettings()}
                            cardStyle={this.state.cardStyle}
                            onCardStyleChanged={this.onCardStyleChanged}
                        /> :
                        <Card
                            ref={this.child}
                            frontContent={cardFront}
                            backContent={cardBack}
                        />}
                </div>
            </div>
        );
    }
}

class App extends Component {

    constructor() {
        super();

        this.cardSupplier = new SentenceCardSupplier(sentences, sentencePieces);
    }

    render() {
        return <CardController cardSupplier={this.cardSupplier}/>
    }
}

export default App;
