// Typing pinyin:
// - Select the "ABC Extended" keyboard
// - Use the following keyboard shortcuts before a vowel for the tone marks:
//   1st tone: ā - Option + A
//   2nd tone: á - Option + E
//   3rd tone: ǎ - Option + V
//   4th tone: à - Option + `
//   U with umlaut with any tone: Tone shortcut + v
//      ǖ - Option + A, then V
//      ǘ - Option + E, then V
//      ǚ - Option + V, then V
//      ǜ - Option + `, then V

// Out of order for use in activities
const activity_favors = [
    { english: "prepare breakfast", chinese: "准备早饭", },
    { english: "make dinner", chinese: "做晚饭", },
    { english: "prepare for a test", chinese: "准备考试", },
    { english: "prepare for Chinese class", chinese: "准备中文课", },
    { english: "practice speaking Chinese", chinese: "练习说中文", },
];

const activities = [
    { english: "dance", chinese: "跳舞", },
    { english: "listen to music", chinese: "听音乐", },
    { english: "play ball", chinese: "打球", },
    { english: "read books", chinese: "看书", },
    { english: "run", chinese: "跑步", },
    { english: "sing", chinese: "唱歌", },
    { english: "watch a movie", chinese: "看电影", },
    { english: "watch television", chinese: "看电视", },
    ...activity_favors,
];

const can_you_answers_no = [
    { english: "Sorry, I'm very busy.", chinese: "对不起，我很忙。", },
    { english: "Sorry, I have no time.", chinese: "对不起，我没空儿。", },
    { english: "Sorry, I don't have time.", chinese: "对不起，我没有时间。", },
];

const can_you_answers_yes = [
    { english: "I can", chinese: "可以", },
    { english: "Ok", chinese: "好的", },
    { english: "No problem", chinese: "没问题", },
];

const can_you_answers_all = [
    ...can_you_answers_yes,
    ...can_you_answers_no,
];

const close_relations = [
    { english: "mother", chinese: "妈妈", },
    { english: "father", chinese: "爸爸", },
    { english: "younger brother", chinese: "弟弟", },
    { english: "younger sister", chinese: "妹妹", },
    { english: "older brother", chinese: "哥哥", },
    { english: "older sister", chinese: "姐姐", },
    { english: "wife", chinese: "太太", },
];

const countries = [
    {
        name: { english: "America", chinese: "美国" },
        nationality: { english: "American", chinese: "美国人" },
        language: { english: "English", chinese: "英语" },
        cuisine: { english: "American food", chinese: "美国菜" },
    },
    {
        name: { english: "Australia", chinese: "澳洲" },
        nationality: { english: "Australian", chinese: "澳洲人" },
        language: { english: "English", chinese: "英语" },
        cuisine: { english: "Australian food", chinese: "澳洲菜" },
    },
    {
        name: { english: "Canada", chinese: "加拿大" },
        nationality: { english: "Canadian", chinese: "加拿大人" },
        language: { english: "English", chinese: "英语" },
        cuisine: { english: "Canadian food", chinese: "加拿大菜" },
    },
    {
        name: { english: "China", chinese: "中国" },
        nationality: { english: "Chinese", chinese: "中国人" },
        language: { english: "Chinese", chinese: "中文" },
        cuisine: { english: "Chinese food", chinese: "中国菜" },
    },
    {
        name: { english: "England", chinese: "英国" },
        nationality: { english: "English", chinese: "英国人" },
        language: { english: "English", chinese: "英语" },
        cuisine: { english: "English food", chinese: "英国菜" },
    },
    {
        name: { english: "France", chinese: "法国" },
        nationality: { english: "French", chinese: "法国人" },
        language: { english: "French", chinese: "法语" },
        cuisine: { english: "French food", chinese: "法国菜" },
    },
    {
        name: { english: "Germany", chinese: "德国" },
        nationality: { english: "German", chinese: "德国人" },
        language: { english: "German", chinese: "德语" },
        cuisine: { english: "German food", chinese: "德国菜" },
    },
    {
        name: { english: "Japan", chinese: "日本" },
        nationality: { english: "Japanese", chinese: "日本人" },
        language: { english: "Japanese", chinese: "日语" },
        cuisine: { english: "Japanese food", chinese: "日本菜" },
    },
    {
        name: { english: "South Korea", chinese: "韩国" },
        nationality: { english: "South Korean", chinese: "韩国人" },
        language: { english: "Korean", chinese: "韩语" },
        cuisine: { english: "Korean food", chinese: "韩国菜" },
    },
];

const days_of_month = [
    { english: "1", chinese: "一号" },
    { english: "2", chinese: "二号" },
    { english: "3", chinese: "三号" },
    { english: "4", chinese: "四号" },
    { english: "5", chinese: "五号" },
    { english: "6", chinese: "六号" },
    { english: "7", chinese: "七号" },
    { english: "8", chinese: "八号" },
    { english: "9", chinese: "九号" },
    { english: "10", chinese: "十号" },
    { english: "11", chinese: "十一号" },
    { english: "12", chinese: "十二号" },
    { english: "13", chinese: "十三号" },
    { english: "14", chinese: "十四号" },
    { english: "15", chinese: "十五号" },
    { english: "16", chinese: "十六号" },
    { english: "17", chinese: "十七号" },
    { english: "18", chinese: "十八号" },
    { english: "19", chinese: "十九号" },
    { english: "20", chinese: "二十号" },
    { english: "21", chinese: "二十一号" },
    { english: "22", chinese: "二十二号" },
    { english: "23", chinese: "二十三号" },
    { english: "24", chinese: "二十四号" },
    { english: "25", chinese: "二十五号" },
    { english: "26", chinese: "二十六号" },
    { english: "27", chinese: "二十七号" },
    { english: "28", chinese: "二十八号" },
];

const days_of_week = [
    { english: "Monday", chinese: "星期一" },
    { english: "Tuesday", chinese: "星期二" },
    { english: "Wednesday", chinese: "星期三" },
    { english: "Thursday", chinese: "星期四" },
    { english: "Friday", chinese: "星期五" },
    { english: "Saturday", chinese: "星期六" },
    { english: "Sunday", chinese: "星期天" },
];

const days_of_week_and_weekend = [
    ...days_of_week,
    { english: "weekend", chinese: "周末" },
];

export const times_early_morning = [
    { english: "5:45am", chinese: "早上6点一刻" },
    { english: "6:00am", chinese: "早上6点" },
    { english: "6:15am", chinese: "早上6点一刻" },
    { english: "6:30am", chinese: "早上6点半" },
    { english: "6:45am", chinese: "早上6点三刻" },
    { english: "7:00am", chinese: "早上6点" },
    { english: "7:30am", chinese: "早上6点半" },
    { english: "7:45am", chinese: "早上6点三刻" },
];

export const times_evening = [
    { english: "7:30pm", chinese: "晚上7点半" },
    { english: "8pm", chinese: "晚上8点" },
    { english: "8:15pm", chinese: "晚上8点一刻" },
    { english: "9pm", chinese: "晚上9点" },
    { english: "9:45pm", chinese: "晚上9点三刻" },
    { english: "10pm", chinese: "晚上10点" },
    { english: "10:30pm", chinese: "晚上10点半" },
    { english: "11pm", chinese: "晚上11点" },
    { english: "11:15pm", chinese: "晚上11点一刻" },
    { english: "11:45pm", chinese: "晚上11点三刻" },
    { english: "midnight", chinese: "晚上12点" },
];

const measured_objects = [
    { english: "a cup of tea", chinese: "一杯茶" },
    { english: "2 pens", chinese: "两枝笔" },
    { english: "3 bottles of water", chinese: "三瓶水" },
    { english: "4 pieces of paper", chinese: "四张纸" },
    { english: "5 photos", chinese: "五张照片" },
    { english: "2 diary entries", chinese: "两篇日记"}
];

const months = [
    { english: "January", chinese: "一月" },
    { english: "February", chinese: "二月" },
    { english: "March", chinese: "三月" },
    { english: "April", chinese: "四月" },
    { english: "May", chinese: "五月" },
    { english: "June", chinese: "六月" },
    { english: "July", chinese: "七月" },
    { english: "August", chinese: "八月" },
    { english: "September", chinese: "九月" },
    { english: "October", chinese: "十月" },
    { english: "November", chinese: "十一月" },
    { english: "December", chinese: "十二月" },
];

const people = [
    {
        full_name: { english: "Winston Gore", chinese: "高文中" },
        subject_pronoun: { english: "he", chinese: "他" },
        object_pronoun: { english: "him", chinese: "他" },
        possessive: { english: "his", chinese: "他" },
        first_name: { english: "Winston", chinese: "文中" },
        family_name: { english: "Gore", chinese: "高" },
    },
    {
        full_name: { english: "Amy Lee", chinese: "李友" },
        subject_pronoun: { english: "she", chinese: "她" },
        object_pronoun: { english: "her", chinese: "她" },
        possessive: { english: "her", chinese: "她" },
        first_name: { english: "Amy", chinese: "友" },
        family_name: { english: "Lee", chinese: "李" },
    },
    {
        full_name: { english: "Peng Wang", chinese: "王朋" },
        subject_pronoun: { english: "he", chinese: "他" },
        object_pronoun: { english: "him", chinese: "他" },
        possessive: { english: "his", chinese: "他" },
        first_name: { english: "Peng", chinese: "朋" },
        family_name: { english: "Wang", chinese: "王" },
    },
    {
        full_name: { english: "Jenny Gore", chinese: "高小音" },
        subject_pronoun: { english: "she", chinese: "她" },
        object_pronoun: { english: "her", chinese: "她" },
        possessive: { english: "her", chinese: "她" },
        first_name: { english: "Jenny", chinese: "小音" },
        family_name: { english: "Gore", chinese: "高" },
    },
];

const people_with_me = [
    {
        full_name: { english: "Brent Plump", chinese: "朴博仁" },
        subject_pronoun: { english: "I", chinese: "我" },
        object_pronoun: { english: "me", chinese: "我" },
        possessive: { english: "my", chinese: "我" },
        first_name: { english: "Brent", chinese: "博仁" },
        family_name: { english: "Plump", chinese: "朴" },
    },
    ...people,
];

const phone_numbers = [
    { english: "0478 931 210", chinese: "零四七八 几三幺 二幺零", },
    { english: "512-244-0585", chinese: "五幺二 二四四 零五八五", },
    { english: "503-740-9603", chinese: "五零三 七零三 七六零三", },
];

const professions = [
    { english: "student", chinese: "学生" },
    { english: "teacher", chinese: "老师" },
    // { english: "engineer", chinese: "" },
    // { english: "architect", chinese: "" },
    // { english: "lawyer", chinese: "" },
    // { english: "doctor", chinese: "" },
    // { english: "manager", chinese: "" },
    // { english: "", chinese: "" },
    // { english: "", chinese: "" },
    // { english: "", chinese: "" },
    // { english: "", chinese: "" },
];

const work_locations = [
    { english: "a school", chinese: "学校", },
    { english: "a library", chinese: "图书馆", },
    { english: "a company", chinese: "公司", },
    { english: "a shop", chinese: "商店", },
    { english: "a restaurant", chinese: "饭店", },
    // { english: "a government agency", chinese: "政府", },
    // { english: "a hospital", chinese: "医院", },
];

export const sentencePieces = {
    activity: activities,
    activity_favor: activity_favors,
    can_you_answer_yes: can_you_answers_yes,
    can_you_answer_no: can_you_answers_no,
    can_you_answer_all: can_you_answers_all,
    close_relation: close_relations,
    country: countries,
    day_of_month: days_of_month,
    day_of_week: days_of_week,
    day_of_week_and_weekend: days_of_week_and_weekend,
    time_early_morning: times_early_morning,
    time_evening: times_evening,
    measured_object: measured_objects,
    month: months,
    person: people,
    person_with_me: people_with_me,
    phone_number: phone_numbers,
    profession: professions,
    work_location: work_locations,
};

export const sentences = [
    // Introduction
//     {
//         english: "Is it right/correct?",
//         chinese: "对不对？",
//     },
//     {
//         english: "Correct",
//         chinese: "对",
//     },
//     {
//         english: "Do you understand?",
//         chinese: "懂不懂？",
//     },
//     {
//         english: "I don't understand.",
//         chinese: "我不懂。",
//     },
//     {
//         english: "I am sorry.",
//         chinese: "对不起。",
//     },
//     {
//         english: "No worries./It's OK.",
//         chinese: "没关系",
//         notes: "Always in response to 对不起。",
//     },
//     {
//         english: "What is this?",
//         chinese: "这是什么？",
//     },
//     {
//         english: "How do you say \"...\" in Chinese?",
//         chinese: "\"...\" 中文怎么说？",
//     },
//     {
//         english: "What does \"...\" mean?",
//         chinese: "\"...\" 是什么意思？",
//     },
//     {
//         english: "Hello, everyone!",
//         chinese: "大家好!",
//     },
//     {
//         english: "Hello!",
//         chinese: "你好!",
//     },
//     {
//         english: "Thank you!",
//         chinese: "谢谢!",
//     },

    // Lesson 1.1
//     {
//         english: "May I please ask your surname?",
//         chinese: "请问你贵姓？",
//     },
//     {
//         english: "What is your surname?",
//         chinese: "你姓什么？(你贵姓？）",
//     },
//     {
//         english: "Is {person:possessive} surname {person:family_name}?",
//         chinese: "{person:possessive}姓{person:family_name}吗？",
//     },
//     {
//         english: "{person_with_me:possessive} surname is {person_with_me:family_name}.",
//         chinese: "{person_with_me:possessive}姓{person_with_me:family_name}。",
//     },
//     {
//         english: "{person_with_me#1:possessive} surname is not {person_with_me#2:family_name}.",
//         chinese: "{person_with_me#1:possessive}不姓{person_with_me#2:family_name}。",
//     },
//     {
//         english: "And you?",
//         chinese: "你呢？",
//     },
//     {
//         english: "What is {person:possessive} name?",
//         chinese: "{person:possessive}叫什么名字？",
//     },
//     {
//         english: "{person_with_me:possessive} name is {person_with_me:full_name}.",
//         chinese: "{person_with_me:possessive}叫{person_with_me:full_name}。",
//     },
//     {
//         english: "Is {person:possessive} name {person:full_name}?",
//         chinese: "{person:possessive}叫{person:full_name}吗？",
//     },

    // Lesson 1.2
//     {
//         english: "Is she {country:nationality}?",
//         chinese: "她是{country:nationality}吗？",
//     },
//     {
//         english: "He is {country:nationality}. I am also {country:nationality}.",
//         chinese: "他是{country:nationality}。我也是{country:nationality}。",
//     },
//     {
//         english: "Which country are you from?",
//         chinese: "你是哪国人？",
//     },

    // Lesson 2.2
//     {
//         english: "Are you a {profession}?",
//         chinese: "你是{profession}吗？",
//     },
//     {
//         english: "I am a {profession}.",
//         chinese: "我是{profession}。",
//     },
//     {
//         english: "He is not a {profession}. I am also not a {profession}.",
//         chinese: "他不是{profession}。我也不是{profession}。",
//     },

    // Lesson 3.1
//     {
//         english: "When is {person:possessive} birthday?",
//         chinese: "{person:possessive}的生日是几月几号？",
//     },
//     {
//         english: "{person_with_me:possessive} birthday is on {day_of_month} {month}",
//         chinese: "{person_with_me:possessive}的生日是{month}{day_of_month}。",
//     },

    // Lesson 3.2
//     {
//         english: "I'll take you out to dinner on {day_of_week}. How's that?",
//         chinese: "我{day_of_week}请你吃晚饭。怎么样？",
//     },
//     {
//         english: "Are you taking me out to dinner on {day_of_week#1} or {day_of_week#2}?",
//         chinese: "你{day_of_week#1}还是{day_of_week#2}请我吃晚饭？",
//     },
//     {
//         english: "I don't like {person:full_name} but I like {person:possessive} {close_relation}.",
//         chinese: "我不喜欢{person:full_name}可是我喜欢{person:possessive}{close_relation}。",
//     },
//     {
//         english: "She's not {country:nationality}, but she enjoys eating {country:cuisine}.",
//         chinese: "她不是{country:nationality}可是她喜欢吃{country:cuisine}。",
//     },

    // Lesson 4
//     {
//         english: "{person_with_me:subject_pronoun} like(s) {activity}.",
//         chinese: "{person_with_me:subject_pronoun}喜欢{activity}。",
//     }

    // Lesson 5.1
//     {
//         english: "Who is it? \nIt's me and {person:full_name}",
//         chinese: "谁呀？\n是我还有{person:full_name}。",
//     },
//     {
//         english: "Please enter! Quickly, come in!",
//         chinese: "请进，快进来！",
//     },
//     {
//     TODO: replace "friend" with relations like "colleague", "student", "teacher", etc
//         english: "Let me introduce you. This is my friend, {person:full_name}.",
//         chinese: "我介绍一下，这是我的朋友，{person:full_name}。",
//     },
//     {
//         english: "Pleased to meet you!",
//         chinese: "认识你很高兴！",
//     },
//     {
//         english: "Pleased to meet you, too!",
//         chinese: "认识你我也很高兴！",
//     },
//     {
//         english: "Your home is very big and beautiful.",
//         chinese: "你们家很大，也很漂亮。",
//     },
//     {
//         english: "Oh? / Really?",
//         chinese: "是吗？",
//     },
//     {
//         english: "Please, sit!",
//         chinese: "请坐！",
//     },
//     {
//         english: "{person:first_name}, where do you work?/nI work at {work_location}.",
//         chinese: "{person:first_name}, 你在哪儿工作？/n我在{work_location}工作。",
//     },

    // Lesson 5.2
//     {
//         english: "He didn't come home until {time_evening} last night.",
//         chinese: "他昨天{time_evening}才回家。",
//     },
//     {
//         english: "He didn't have dinner until {time_evening} last night.",
//         chinese: "他昨天{time_evening}才吃饭。",
//     },
//     {
//         english: "Last night I didn't go to bed until {time_evening}.",
//         chinese: "我昨天{time_evening}才睡觉。",
//     },
//     {
//         english: "{person_} {close_relation} often comes home late.",
//         chinese: "我{close_relation}常常很晚才回家。",
//     },
//     {
//         english: "Today, Sam didn't go to school until 10am.",
//         chinese: "Sam今天10点才学校。",
//     },
//     {
//         english: "Yesterday, Tom didn't come home until 11pm.",
//         chinese: "Tom昨天晚上11点才回家。",
//     },
//     {
//         // TODO: replace with activity (but I need past tense)
//         english: "What did you do this weekend?\nI watched a movie. And you?",
//         chinese: "你周末做什么了？\n我看电影了。你呢？",
//     },
//     {
//         english: "Did you study Chinese yesterday?\nYup.",
//         chinese: "你昨天学中文了吗？\n学了。",
//     },
//     {
//         english: "Did you eat dinner?\nI didn't.",
//         chinese: "你吃晚饭了吗？\n没吃。",
//     },
//     {
//         english: "How many cups of coffee did you drink yesterday?\nI drank 3 cups.",
//         chinese: "你昨天喝了几杯咖啡？\n喝了三杯。",
//     },
//     {
//         english: "Did you read any books this weekend?\nYes.",
//         chinese: "你周末看书了吗？\n看书了。",
//     },

//     // Lesson 6.1
//     {
//         english: "I'll call you tomorrow.",
//         chinese: "明天我给你打电话。",
//     },
//     {
//         english: "He gave me a call.",
//         chinese: "他给我打了一个电话。",
//     },
//     {
//         english: "Please give her a phone call.",
//         chinese: "请给她打电话。",
//     },
//     {
//         english: "The teacher gives us classes.",
//         chinese: "老师给我们上课。",
//     },
//     {
//         english: "I sing for mom.",
//         chinese: "我给妈妈唱歌。",
//     },
//     {
//         english: "Who is he? Please introduce us.",
//         chinese: "他是谁？请你给我们介绍一下。",
//     },
//     {
//         english: "I introduce a Chinese friend to my classmate.",
//         chinese: "我给同学介绍中国朋友。",
//     },
//     {
//         english: "I show her my photo.",
//         chinese: "我给她看我的照片。",
//     },
//     {
//         english: "{person_with_me#1:full_name} gives {person_with_me#2:full_name} a phone call.",
//         chinese: "{person_with_me#1:full_name}给{person_with_me#2:full_name}打电话。",
//     },
//     {
//         english: "{person_with_me#1:full_name} calls {person_with_me#2:full_name} on the phone.",
//         chinese: "{person_with_me#1:full_name}给{person_with_me#2:full_name}打电话。",
//     },
//     {
//         english: "Do you have a picture of your older sister? Can you let me have a look?",
//         chinese: "你有你姐姐的照片吗？给我看一下，行吗？",
//     },
//     {
//         english: "Do you call your {close_relation} often?",
//         chinese: "你常常给你{close_relation}打电话吗?",
//     },
//     {
//         english: "Who do you call often?\nMy {close_relation}.",
//         chinese: "你常常给谁打电话？\n我{close_relation}。",
//     },
//     {
//         // TODO: Get the voice to read phone numbers correctly (with yāo instead of yī)
//         english: "Can I call you tomorrow?\nYou can.\nWhat's your number?\nMy number is {phone_number}.",
//         chinese: "我明天可以给你打电话吗？\n可以。\n你的电话是多少？\n我的电话是{phone_number}。"
//     },

    // Lesson 6.2
//     {
//         english: "What are you going to do next {day_of_week_and_weekend}?\nI will {activity}.",
//         chinese: "你下个{day_of_week_and_weekend}要做什么？\n我要{activity}。",
//     },
//     {
//         english: "Do you often {activity}?",
//         chinese: "你常常{activity}吗？",
//     },
//     {
//         english: "Next week I have a test, can you help me prepare for it?\n{can_you_answer_all}",
//         chinese: "我下个星期有考试，你可以帮我准备一下吗? \n{can_you_answer_all}",
//     },
//     {
//         english: "Next {day_of_week_and_weekend}, can you help me {activity_favor}?\n{can_you_answer_no}\nThen when do you have time?",
//         chinese: "你下个{day_of_week_and_weekend}可以帮我{activity_favor}一下吗?\n{can_you_answer_no}\n那你什么时候有时间？",
//     },

    // Lesson 7.1
//    {
//        english: "How did you do on the exam last week?",
//        chinese: "你上个星期考试考得怎么样？",
//    },
//    {
//        english: "{person_with_me#1:subject_pronoun} talks with {person_with_me#2:full_name}.",
//        chinese: "{person_with_me#1:subject_pronoun}跟{person_with_me#2:full_name}说话。",
//    },
//    {
//        english: "Because you helped me review, the test wasn't bad.",
//        chinese: "因为你帮我复习，所以考得不错。",
//    },
//    {
//        english: "I write Chinese characters very slowly.",
//        chinese: "我写汉字写的太慢了。",
//    },
//    {
//        english: "I will practice writing with you later. Good?",
//        chinese: "以后我跟你一起练习写字。好不好？",
//    },
//    {
//        english: "He has to go to class at 7am tomorrow!",
//        chinese: "他明天早上七点就得上课！",
//    },
//    {
//        english: "We watched the 8pm movie but he came at 7:30!",
//        chinese: "我们八点看电影，他七点半就来了！",
//    },
//    {
//        english: "Give me {measured_object#1} and {measured_object#2}.",
//        chinese: "给我{measured_object#1}，{measured_object#2}。",
//    },
//    {
//        english: "Teach me how to write the character for \"wǒ\", please.",
//        chinese: "你教我怎么写\"我\"字吧。",
//    },
//    {
//        english: "You write characters really well and really fast!\nNot at all!",
//        chinese: "你写字写的真好，真快！\n哪里，哪里。",
//    },

    // Lesson 8
//     {
//         english: "Yesterday, I was very busy and very tired.",
//         chinese: "昨天我很忙，很累。",
//     },
//     {
//         english: "I woke up at {time_early_morning}.",
//         chinese: "{time_early_morning}起床。",
//     },

    // Lesson 11
    {
        english: "Today is colder than yesterday.",
        chinese: "今天比昨天冷。",
    },
    {
        english: "This character is easier than that one.",
        chinese: "这个字比那个容易。",
    },
    {
        english: "It is more convenient to take a train than a bus.",
        chinese: "坐火车比坐汽车方便。",
    },
    {
        english: "This shirt is cheaper than that one.",
        chinese: "这件衬衫比那件便宜。",
    },
    {
        english: "I think Chinese is easier than English. How about you?",
        chinese: "我觉得中文比英文容易。 你呢？",
    },
    {
        english: "Tomorrow will be a little warmer than today.",
        chinese: "明天会比今天暖一点儿。",
    },
    {
        english: "These pants are much more expensive than those.",
        chinese: "这条裤子比那条贵得多。",
    },
    {
        english: "The weather today is better than yesterday, it's not snowing.",
        chinese: "今天天气比昨天好，不下雪了。",
    },
    {
        english: "I asked my friend to go ice skating in the park tomorrow. I don't know what the weather will be like?",
        chinese: "我约了朋友明天去公园滑冰，不知道天气会怎么样？",
    },
    {
        english: "I just read the weather forecast online, and the weather tomorrow is better than today. Not only will it not snow, but it will be a little warmer.",
        chinese: "我刚才看了网上的天气预报，明天天气比今天更好。不但不会下雪，而且会暖和一点儿。",
    },
    {
        english: "Who did you ask to go skating?",
        chinese: "你约了谁去滑冰?",
    },
    {
        english: "You asked bai Ying'ai? She flew to New York this morning.",
        chinese: "你约了白英爱?可是她今天 早上坐飞机去纽约了。",
    },
    {
        english: "Really? What shall I do tomorrow?",
        chinese: "真的啊?那我明天怎么办?",
    },
    {
        english: "You should still watch a DVD at home!",
        chinese: "你还是在家看碟吧!",
    },
];
