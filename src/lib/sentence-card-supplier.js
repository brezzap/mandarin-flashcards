import {createCard, RandomBag} from "./utils";

export class SentenceCardSupplier {

    constructor(templates, pieces) {
        this.templates = new RandomBag(templates);

        // Map the values of pieces into RandomBags, too
        this.pieces = new Map();
        Object.getOwnPropertyNames(pieces).forEach(key => {
            this.pieces[key] = new RandomBag(pieces[key]);
        });
    }

    getNextCard() {
        const sentence = this.templates.next;

        const tokenizedEnglish = tokenize(sentence.english);
        const tokenizedChinese = tokenize(sentence.chinese);

        // Build the English sentence and select the values for the tokens
        let englishString = '';
        const chosenInstances = {};
        const chineseValues = {};
        for (let fragment of tokenizedEnglish) {
            switch (fragment.type) {
                case 'token':
                    let token = fragment.value;
                    // Token format: type[#1][:specifier]
                    // type       - the type of piece
                    // #1         - an instance number (required when there are multiple tokens of the same type)
                    // :specifier - for multi-valued objects, the specific value to use

                    let instanceId = token.includes(':') ? token.split(':')[0] : token;

                    let piece;
                    // If we've already chosen an instance, get that...
                    if (chosenInstances.hasOwnProperty(instanceId)) {
                        piece = chosenInstances[instanceId];
                    } else {
                        let type = instanceId.includes('#') ? instanceId.split('#')[0] : instanceId;
                        piece = this.pieces[type].next;
                        chosenInstances[instanceId] = piece;
                    }

                    let specificPiece;
                    if (token.includes(':')) {
                        const specificPieceType = token.split(':')[1];
                        specificPiece = piece[specificPieceType];
                    } else {
                        specificPiece = piece;
                    }

                    chineseValues[token] = specificPiece.chinese;
                    englishString += specificPiece.english;
                    break;
                default:
                    // assume string type
                    englishString += fragment.value;
            }
        }
        englishString = capitalizeFirstLetter(englishString);


        // Now build the chinese string with Chinese versions of the token values used to build the English string
        // We don't need to worry about the sub-values because chineseValues is indexed by the full value string
        let chineseString = '';
        for (let fragment of tokenizedChinese) {
            switch (fragment.type) {
                case 'token':
                    let token = fragment.value;
                    let value = chineseValues[token];
                    if (value) {
                        chineseString += value;
                    } else {
                        chineseString += `${token}`;
                    }
                    break;
                default:
                    // assume string type
                    chineseString += fragment.value;
            }
        }

        return createCard(englishString, chineseString);
    }
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function tokenize(str) {
    const data = [];
    var state = 'string';
    var capture = '';
    for (var char of str) {
        switch (state) {
            case 'string':
                if (char === '{') {
                    data.push({
                        type: 'string',
                        value: capture,
                    });
                    state = 'token';
                    capture = '';
                } else {
                    capture = capture + char;
                }
                break;
            case 'token':
                if (char === '}') {
                    data.push({
                        type: 'token',
                        value: capture,
                    });
                    state = 'string';
                    capture = '';
                } else {
                    capture = capture + char;
                }
                break;
            default:
                // something's wrong
                data.push({
                    type: 'string',
                    value: 'ERROR',
                });
                state = 'string';
        }
    }
    // Finish off...
    switch (state) {
        case 'string':
            if (capture !== '') {
                data.push({
                    type: 'string',
                    value: capture,
                });
            }
            break;
        default:
            // something's wrong
            data.push({
                type: 'string',
                value: 'ERROR',
            });
    }

    return data;
}
