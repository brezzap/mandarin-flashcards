export function createCard(english, chinese, pinyin, notes) {
    return ({
        english,
        chinese,
        pinyin,
        notes,
    });
}

// Given an array, mix it up and ensure we've returned
// each value once before we re-shuffle the values again
export class RandomBag {
    constructor(array) {
        this.values = array;
        this.nextIndex = this.values.length;
    }

    get next() {
        if (this.nextIndex >= this.values.length) {
            shuffle(this.values);
            this.nextIndex = 0;
        }
        return this.values[this.nextIndex++];
    }
}

function shuffle(array) {
    var j, x, i;
    for (i = array.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = array[i];
        array[i] = array[j];
        array[j] = x;
    }
}
