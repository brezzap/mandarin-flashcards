// I'm not suing this stuff. Pleco is better for learning new vocabulary.

import {createCard, RandomBag} from "./utils";

export class ArrayCardSupplier {

    constructor(cardArray) {
        this.cards = new RandomBag(cardArray.map(cardData => createCard(cardData[0], cardData[2], cardData[1], cardData[3])));
    }

    getNextCard() {
        return this.cards.next;
    }
}

const numbers = [
    ["0", "líng", "〇"],
    ["1", "yī (yāo)", "一"],
    ["2", "èr", "二"],
    ["3", "sān", "三"],
    ["4", "sì", "四"],
    ["5", "wǔ", "五"],
    ["6", "liù", "六"],
    ["7", "qī", "七"],
    ["8", "bā", "八"],
    ["9", "jiǔ", "九"],
    ["10", "shí", "十"],
    ["11", "shíyī", "十一"],
    ["12", "shíèr", "十二"],
    ["13", "shísān", "十三"],
    ["16", "shíliù", "十六"],
    ["17", "shíqī", "十七"],
    ["19", "shíjiǔ", "十九"],
    ["20", "èrshí", "二十"],
    ["24", "èrshísì", "二十四"],
    ["25", "èrshíwǔ", "二十五"],
    ["26", "èrshíliù", "二十六"],
    ["28", "èrshíbā", "二十八"],
    ["29", "èrshíjiǔ", "二十九"],
    ["30", "sānshí", "三十"],
    ["33", "sānshísān", "三十三"],
    ["36", "sānshíliù", "三十六"],
    ["39", "sānshíjiǔ", "三十九"],
    ["100", "yībǎi", "一百"],
    ["400", "sìbǎi", "四百"],
    ["1,000", "yīqiān", "一千"],
    ["7,000", "qīqiān", "七千"],
    ["10,000", "yīwàn ", "一万", "1'0000"],
    ["60,000", "liùwàn ", "六万", "6'0000"],
    ["100,000", "shíwàn", "十万", "10'0000"],
    ["900,000", "jiǔshíwàn", "九十万", "90'0000"],
    ["1,000,000", "yībǎiwàn", "一百万", "100'0000"],
    ["5,000,000", "wǔbǎiwàn", "五百万", "500'0000"],
];

const years = [
    ["1215 (year)", "yīèryīwǔnián", "1215年", "Magna Carta"],
    ["1776 (year)", "yīqīqīliùnián", "1776年", "US independence declared"],
    ["1788 (year)", "yīqībābānián", "1788年", "First fleet landed"],
    ["1901 (year)", "yījiǔlíngyīnián", "1901年", "Australia federated"],
    ["1912 (year)", "yījiǔyīèrnián", "1912年", "Chinese republic established"],
    ["1930 (year)", "yījiǔsānlíngnián", "1930年", "Great depression"],
    ["1945 (year)", "yījiǔsìwǔnián", "1945年", "WWII ended"],
    ["2003 (year)", "èrlínglíngsān", "2003年", "Columbia disaster"],
    ["2019 (year)", "èrlíngyījiǔnián", "2019年"],
];

const dates = [
    ["Saturday, 7 September 1901", "", "1901年9月7日星期六", "Boxer Rebellion ends"],
    ["Saturday, 1 October 1949", "", "1949年10月1日星期六", "People's Republic of China established by Mao"],
    ["Monday, 28 February 1972", "", "1972年2月28日星期一", "Nixon visits China"],
    ["Tuesday, 1 July 1997", "", "1997年7月1日星期二", "Handover of Hong Kong"],
    ["Wednesday, 15 October 2003", "", "2003年10月15日星期三", "China launches its first astronaut into space (Yang Liwei)"],
    ["Thursday, 14 March 2013", "", "2013年3月14日星期四", "Xi Jinping becomes president"],
];

const lesson1part1 = [
    ["to ask (a question)", "wèn", "问"],
    ["honorable; expensive", "guì", "贵"],
    ["surname; to be surnamed", "xìng", "姓"],
    ["I; me", "wǒ", "我"],
    ["(form a question when the context is clear)", "ne", "呢", "Follows a noun or pronoun to form a question when the context is clear"],
    ["Miss; young lady", "xiǎojiě", "小姐"],
    ["to be called; to call", "jiào", "叫"],
    ["what", "shénme", "什么"],
    ["name", "míngzi", "名字"],
    ["Mr.; husband; teacher", "xiānsheng", "先生"],
    ["You Li", "Lǐ Yǒu", "李友"],
    ["Peng Wang", "Wáng Péng", "王朋"],
    ["Brent Plump", "Piáo Bórén", "朴博仁"],
    ["plum", "lǐ", "李"],
    ["king", "wáng", "王"],
];

const lesson1part2 = [
    ["to be", "shì", "是"],
    ["teacher", "lǎoshī", "老师"],
    ["(form a question from a statement)", "ma", "吗", "Add to the end of a declarative statement to form a question"],
    ["not; no", "bù", "不", "Negative adverb - comes before a verb. Comes after yě: 我也不是。"],
    ["student", "xuésheng", "学生"],
    ["too; also", "yě", "也", "Adverb - comes before a verb. Comes before bù: 我也不是。"],
    ["people; person", "rén", "人"],

    // Cities (alphabetized)
    ["Beijing", "Běijīng", "北京"],
    ["London", "Lúndūn", "伦敦"],
    ["New York", "Niǔyuē", "纽约"],
    ["Sydney", "Xīní", "悉尼"],
];

const lesson2part1 = [
    ["he; him", "tā", "他"],
    ["she; her", "tā", "她"],
    ["(possessive/descriptive particle)", "de", "的"],
    // TODO: This is a partial list
];

const lesson3part1 = [
    ["month", "yuè", "月"],
    ["(measure word for number in a series; day of the month)", "hào", "号"],
    ["week", "xīngqī", "星期"],
    ["day", "tiān", "天"],
    ["birthday", "shēngrì", "生日"],
    ["to give birth to; to be born", "shēng", "生"],
    ["day; sun", "rì", "日"],
    ["this year", "jīnnián", "今年"],
    ["year", "nián", "年"],
    ["how many/much; to what extent; many", "duō", "多"],
    ["big; old", "dà", "大"],
    ["year (of age)", "suì", "岁"],
    ["to eat", "chī", "吃"],
    ["meal; (cooked) rice", "fàn", "饭"],
    ["OK? How's that sound?", "zěnmeyàng", "怎么样"],
    ["too; extremely", "tài...le", "太...了"],
    ["to thank", "xièxie", "谢谢"],
    ["to like", "xǐhuan", "喜欢"],
    ["dishes, cuisine", "cài", "菜"],
    ["or", "háishi", "还是"],
    ["but", "kěshì", "可是"],
    ["we", "wǒmen", "我们"],
    ["American food", "Měiguó cài", "美国菜"],
    ["Chinese food", "Zhōngguó cài", "中国菜"],
];

export const cardData = [
    ...numbers,
    ...lesson1part1,
    ...lesson1part2,
    ...lesson2part1,
    ...lesson3part1,
    ...years,
    ...dates,
];