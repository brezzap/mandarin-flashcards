#!/bin/bash 

# Deploy to Bitbucket static hosting via a git repo
DEPLOY_GIT_DIR=/files/code/brezzzap.bitbucket.io/
DEPLOY_SUB_DIR=mandarin-flashcards

set -euf -o pipefail

rsync -a --delete "build/" "${DEPLOY_GIT_DIR}/mandarin-flashcards/"
cd "${DEPLOY_GIT_DIR}"
if [ -z "$(git status --porcelain)" ]; then 
  echo "No changes to commit"
else
  git add --all
  git commit -m "Deploying new version of ${DEPLOY_SUB_DIR}"
  # TODO: Probably shouldn't assume we're already on master but ¯\_(ツ)_/¯
  git push origin master
fi